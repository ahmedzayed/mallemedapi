﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
   public class CategorysVm
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public string Icon { get; set; }
        public int Counts { get; set; }

    }
}
