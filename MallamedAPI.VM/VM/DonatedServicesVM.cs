﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
  public  class DonatedServicesVM
    {
        public int ID { get; set; }
        public Nullable<int> DonatorID { get; set; }
        public string Service { get; set; }
        public Nullable<int> Discount { get; set; }
    }
}
