﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
   public class DonatorBranchesVM
    {
        public int ID { get; set; }
        public int DonatorID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Lang { get; set; }
        public string Lat { get; set; }
    }
}
