﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
   public class DonatorContactsVM
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public int DonatorID { get; set; }
        public string Name { get; set; }
    }
}
