﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
   public class DonatorImagesVM
    {
        public int ID { get; set; }
        public int DonatorID { get; set; }
        public string Image { get; set; }
        public bool IsActive { get; set; }
    }
}
