﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
  public  class EventPhotoVM
    {
        public int Id { get; set; }
        public string PhotoPath { get; set; }
        public bool IsSuggestion { get; set; }
        public string Event { get; set; }
        public string Cards { get; set; }
    }
}
