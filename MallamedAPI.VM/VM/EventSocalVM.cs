﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
   public class EventSocalVM
    {
        public int Id { get; set; }
        public string Event { get; set; }
        public string Link { get; set; }
        public string type { get; set; }
        public bool IsActive { get; set; }
    }
}
