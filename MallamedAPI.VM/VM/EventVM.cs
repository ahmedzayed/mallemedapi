﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
 public   class EventVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PhotoPath { get; set; }
        public string Address { get; set; }
        public string Region { get; set; }
        public string EventType { get; set; }
        public int EventTypeId { get; set; }
        public string Lat { get; set; }
        public string Lang { get; set; }
        public string Reference { get; set; }
    }
}
