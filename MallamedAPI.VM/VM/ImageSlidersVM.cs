﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
   public class ImageSlidersVM
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public string PhotoPath { get; set; }
        public bool IsActive { get; set; }
    }
}
