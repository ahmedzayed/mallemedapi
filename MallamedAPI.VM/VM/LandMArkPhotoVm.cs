﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
   public class LandMArkPhotoVm
    {
        public int Id { get; set; }
        public string PhotoPath { get; set; }
        public bool IsSuggestion { get; set; }
        public string LandMark { get; set; }
        public string Cards { get; set; }
    }
}
