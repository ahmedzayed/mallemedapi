﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
  public  class LandMArkTypeVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Counts { get; set; }
    }

}
