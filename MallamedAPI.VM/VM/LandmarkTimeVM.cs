﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
   public class LandmarkTimeVM
    {
        public int Id { get; set; }
        public string ValidTimeTo { get; set; }
        public string ValidTimeFrom { get; set; }
        public string ValidDayFrom { get; set; }
        public string ValidDayTo { get; set; }
        public string Landmark { get; set; }
    }
}
