﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallamedAPI.VM.VM
{
   public class SearchVM
    {
        public List<DonatorsVM> Donators { get; set; }
        public List<LandMarksVm> LandMarks { get; set; }
        public List<EventVM> Events { get; set; }

    }
}
