﻿using MallamAPI.Core;
using MallamedAPI.VM.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MallemedAPI.Controllers
{


[System.Web.Http.RoutePrefix("api/Department")]
    public class DepartmentController : ApiController
    {
        string BaseURL = "http://dalelalmoalem.com";

        [System.Web.Http.HttpGet, System.Web.Http.Route("GetBaseUrl")]
        public HttpResponseMessage GetBaseUrl()
        {
            return Request.CreateErrorResponse(HttpStatusCode.OK, BaseURL);
        }

     
        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllcondtions")]
        public HttpResponseMessage GetAllcondtions()
        {

            var model = "SP_GetAllcondition".ExecuParamsSqlOrStored(false).AsList<CondtionVM>();
          
            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }
        [System.Web.Http.HttpGet, System.Web.Http.Route("AboutApp")]
        public HttpResponseMessage AboutApp()
        {

            var model = "SP_GetAllAppDetails".ExecuParamsSqlOrStored(false).AsList<AppDetails>();

            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }


        [System.Web.Http.HttpGet, System.Web.Http.Route("ContactUs")]
        public HttpResponseMessage ContactUs(string Name ,string Phone ,string Email, string Message ,string Address )
        {

            var model = "SP_AddContact".ExecuParamsSqlOrStored(false, "Name".KVP(Name), "Phone".KVP(Phone),"Email".KVP(Email),"Message".KVP(Message),"Address".KVP(Address),"Id".KVP(0)).AsNonQuery();

            if (model != 0)
            {
                Result R = new Result();
                R.Massage = "تم ارسال رسالتك بنجاح";
                R.Status = true;
                return Request.CreateResponse(R);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لم يتم ارسال الرساله";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("AddTokens")]
        public HttpResponseMessage AddTokens(string Token)
        {

            var model = "SP_AddToken".ExecuParamsSqlOrStored(false, "Token".KVP(Token), "Id".KVP(0)).AsNonQuery();

            if (model != 0)
            {
                Result R = new Result();
                R.Massage = "تم  الاضافة بنجاح";
                R.Status = true;
                return Request.CreateResponse(R);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لم يتم  الاضافة";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }


        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllDonators")]
        public HttpResponseMessage GetAllDonators()
        {
            var model = "SP_GetAllCategorysAPI".ExecuParamsSqlOrStored(false).AsList<CategorysVm>();
               
            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllDonatorsById")]
        public HttpResponseMessage GetAllDonatorsById(int? CatId)
        {
            //var cat = db.Categorys.Where(x => x.ID == DonatorId).FirstOrDefault();

            var model = "SP_GetAlldonatorsAPI".ExecuParamsSqlOrStored(false).AsList<DonatorsVM>();
            model = model.Where(a => a.CatID == CatId).ToList();
            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }
        [System.Web.Http.HttpGet, System.Web.Http.Route("DonatorDetails")]
        public HttpResponseMessage DonatorDetails(int? DonatorId)
        {

            var model = "SP_GetAlldonatorsAPI".ExecuParamsSqlOrStored(false).AsList<DonatorsVM>();

             var model1 = model.Where(a => a.ID == DonatorId).FirstOrDefault();

            var DonatedServices= "SP_GetAlldonatorservicesAPI".ExecuParamsSqlOrStored(false,"DonatorId".KVP(DonatorId)).AsList<DonatedServicesVM>();
            var DonatorBranches = "SP_GetAllDonatorBranchesAPI".ExecuParamsSqlOrStored(false, "DonatorId".KVP(DonatorId)).AsList<DonatorBranchesVM>();
            var DonatorContacts = "SP_GetAllDonatorContactsAPI".ExecuParamsSqlOrStored(false, "DonatorId".KVP(DonatorId)).AsList<DonatorContactsVM>();
            var DonatorImages = "SP_GetAllDonatorImagesAPI".ExecuParamsSqlOrStored(false, "DonatorId".KVP(DonatorId)).AsList<DonatorImagesVM>();

            if (model1 != null)
            {
                DonatorDetailsVM Model = new DonatorDetailsVM();
                Model.Address = model1.Address;
                Model.BookName = model1.BookName;
                Model.Categoires = model1.Categoires;
                Model.CatID = model1.CatID;
                Model.Email = model1.Email;
                Model.ID = model1.ID;
                Model.OrgName = model1.OrgName;
                Model.OwnName = model1.OwnName;
                Model.Phone1 = model1.Phone1;
                Model.Photo = model1.Photo;
                Model.Region = model1.Region;
                Model.ResponsableName = model1.ResponsableName;
                Model.DonatedServicesList = DonatedServices;
                Model.DonatorBranchesList = DonatorBranches;
                Model.DonatorContactsList = DonatorContacts;
                Model.DonatorImagesList = DonatorImages;


                if (Model != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Model);
                }
                else
                {
                    Result R = new Result();
                    R.Massage = "لا يوجد بيانات";
                    R.Status = false;
                    R.Description = "من فضلك تواصل مع المسؤل";
                    return Request.CreateResponse(R);

                }
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);
            }
        }
        
      
        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllLandMarkType")]
        public HttpResponseMessage GetAllLandMarkType()
        {

            var model = "SP_GetAllLandMArkTypeAPI".ExecuParamsSqlOrStored(false).AsList<LandMArkTypeVm>();

            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }


        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllLandMarkById")]
        public HttpResponseMessage GetAllLandMarkById(int? LandMarkId)
        {

            var model = "SP_GetAllLandMarkAPI".ExecuParamsSqlOrStored(false).AsList<LandMarksVm>();
            var model1 = model.Where(a => a.LandmarkId == LandMarkId).ToList();
            if (model1 != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, model1);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("LandMarkDetails")]
        public HttpResponseMessage LandMarkDetails(int? LandMarkId)
        {

            var model = "SP_GetAllLandMarkAPI".ExecuParamsSqlOrStored(false).AsList<LandMarksVm>();
            var model1 = model.Where(a => a.Id == LandMarkId).FirstOrDefault();

            var Socailmedia = "SP_GetAllLandMarkSocilamediaAPI".ExecuParamsSqlOrStored(false, "LandMarkId".KVP(LandMarkId)).AsList<LandmarkscoilmedVM>().ToList();
            var Phones = "SP_GetAllLandMarkPhonesAPI".ExecuParamsSqlOrStored(false, "LandMarkId".KVP(LandMarkId)).AsList<LandmarkPhonesVM>().ToList();
            var Photo = "SP_GetAllLandPhotoAPI".ExecuParamsSqlOrStored(false, "LandMarkId".KVP(LandMarkId)).AsList<LandMArkPhotoVm>().ToList();
            var Worktime = "SP_GetAllLandMarkTimeAPI".ExecuParamsSqlOrStored(false, "LandMarkId".KVP(LandMarkId)).AsList<LandmarkTimeVM>().ToList();

            if (model != null)
            {
                LandMarkDetailsVM mode2 = new LandMarkDetailsVM();
                mode2.AudioPath = model1.AudioPath;
                mode2.CityName = model1.CityName;
                mode2.CopyRight = model1.CopyRight;
                mode2.Details = model1.Details;
                mode2.Email = model1.Email;
                mode2.Id = model1.Id;
                mode2.Landmark = model1.Landmark;
                mode2.LandmarkId = model1.LandmarkId;
                mode2.LatX = model1.LatX;
                mode2.LongX = model1.LongX;
                mode2.Name = model1.Name;
                mode2.Owner = model1.Owner;
                mode2.PhotoPath = model1.PhotoPath;
                mode2.Reference = model1.Reference;
                mode2.Region = model1.Region;
                mode2.VideoPath = model1.VideoPath;
                mode2.SocialList = Socailmedia;
                mode2.PhonesList = Phones;
                mode2.PhotoList = Photo;
                mode2.WorkTimeList = Worktime;



                if (mode2 != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, mode2);
                }
                else
                {
                    Result R = new Result();
                    R.Massage = "لا يوجد بيانات";
                    R.Status = false;
                    R.Description = "من فضلك تواصل مع المسؤل";
                    return Request.CreateResponse(R);

                }
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }
        }




        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllLEvent")]
        public HttpResponseMessage GetAllLEvent()
        {

            var model = "SP_GetAllEventTypeAPI".ExecuParamsSqlOrStored(false).AsList<EventTypeVM>();

            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllLEventById")]
        public HttpResponseMessage GetAllLEventById(int? EventTypeId)
        {

            var model = "SP_GetAllEventsAPI".ExecuParamsSqlOrStored(false, "EventTypeId".KVP(EventTypeId)).AsList<EventVM>();
            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllLEventDetails")]
        public HttpResponseMessage GetAllLEventDetails(int? EventId)
        {

            var model = "SP_GetAllEventsAPIById".ExecuParamsSqlOrStored(false, "Id".KVP(EventId)).AsList<EventVM>().FirstOrDefault();

            var Socailmedia = "SP_GetAllEventSocilamediaAPI".ExecuParamsSqlOrStored(false, "EventId".KVP(EventId)).AsList<EventSocalVM>().ToList();
            var Sponse = "SP_GetAllEventSponsorAPI".ExecuParamsSqlOrStored(false, "EventId".KVP(EventId)).AsList<EventSponserVM>().ToList();
            var Photo = "SP_GetAllEventhotoAPI".ExecuParamsSqlOrStored(false, "EventId".KVP(EventId)).AsList<EventPhotoVM>().ToList();
            if (model != null)
            {
                EventDetailsVM model1 = new EventDetailsVM();
                model1.Address = model.Address;
                model1.Details = model.Details;
                model1.EndDate = model.EndDate;
                model1.EventType = model.EventType;
                model1.EventTypeId = model.EventTypeId;
                model1.Lang = model.Lang;
                model1.Lat = model.Lat;
                model1.Name = model.Name;
                model1.PhotoList = Photo;
                model1.PhotoPath = model.PhotoPath;
                model1.Reference = model.Reference;
                model1.Region = model.Region;
                model1.SocialList = Socailmedia;
                model1.SponserList = Sponse;
                model1.StartDate = model.StartDate;


                if (model1 != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, model1);
                }
                else
                {
                    Result R = new Result();
                    R.Massage = "لا يوجد بيانات";
                    R.Status = false;
                    R.Description = "من فضلك تواصل مع المسؤل";
                    return Request.CreateResponse(R);

                }
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);
            }

        }


        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllSlider")]
        public HttpResponseMessage GetAllSlider()
        {

            var model = "SP_GetAllSlider".ExecuParamsSqlOrStored(false).AsList<ImageSlidersVM>();
            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllNews")]
        public HttpResponseMessage GetAllNews()
        {

            var model = "SP_GetAllNews".ExecuParamsSqlOrStored(false).AsList<NewsVM>();
            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllBySearch")]
        public HttpResponseMessage GetAllBySearch(string Search)
        {


            var Don = "SP_GetAlldonatorsAPISearch".ExecuParamsSqlOrStored(false,"Search".KVP(Search)).AsList<DonatorsVM>();
            var Landmark = "SP_GetAllLandMarkAPISearch".ExecuParamsSqlOrStored(false, "Search".KVP(Search)).AsList<LandMarksVm>();
            var Event = "SP_GetAllEventsSearch".ExecuParamsSqlOrStored(false, "Search".KVP(Search)).AsList<EventVM>();

            SearchVM model = new SearchVM();
            
                
                    model.Donators=Don;
            model.LandMarks = Landmark;
            model.Events = Event;



               
            
          

            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }



    }
}
